import numpy as np
import sys

# Re
Re = round(np.random.uniform(500, 1000), 0)

# b 
b = round(np.random.uniform(0.80, 1.00), 3)

# h 
h = round(np.random.uniform(0.10, 0.16), 3)

# H
H = round(np.random.uniform(5.0, 6.0), 2)


original_stdout = sys.stdout # Save a reference to the original standard output
with open('./params', 'w') as f:
    sys.stdout = f # Change the standard output to the file we created.
    print("Re="+str(Re))
    print("b="+str(b))
    print("h="+str(h))
    print("H="+str(H))
    sys.stdout = original_stdout # Reset the standard output to its original value