import h5py
import numpy as np
import vtk
from vtk.util.numpy_support import vtk_to_numpy
from scipy.interpolate import griddata
import argparse
from xml.etree import ElementTree


# Inputs to the program
parser = argparse.ArgumentParser()
parser.add_argument('h5_file',type=str)
parser.add_argument('Re',type=float)
args = parser.parse_args()

print("Writingfile:", args.h5_file)
h5_data = h5py.File(args.h5_file, 'w')

# Get nodes and edges from xml file
dom = ElementTree.parse("mod_geo.xml")
V = dom.findall('GEOMETRY/VERTEX/V')
edges = dom.findall('GEOMETRY/EDGE/E')
composites = dom.findall('GEOMETRY/COMPOSITE/C')
# Store nodes position
Nv = len(V)
pos = np.zeros((Nv,2))
for i, node in enumerate(V):
    pos[i,:] = node.text.split()[:2]
# Save points coordinates
h5_data.create_dataset('pos', data=pos.astype(np.float32))

for t in range(500,601):
    # Get data from vtu
    vtu_file = "temp_"+str(t)+".vtu"
    reader = vtk.vtkXMLUnstructuredGridReader()
    reader.SetFileName(vtu_file)
    reader.Update()
    data = reader.GetOutput()
    if t == 500:
        points = data.GetPoints()
        x = vtk_to_numpy(points.GetData())[:,:2]
    u  = vtk_to_numpy(data.GetPointData().GetArray(0))
    v  = vtk_to_numpy(data.GetPointData().GetArray(1))
    p  = vtk_to_numpy(data.GetPointData().GetArray(2))
    px = vtk_to_numpy(data.GetPointData().GetArray(3))
    py = vtk_to_numpy(data.GetPointData().GetArray(4))
    s  = vtk_to_numpy(data.GetPointData().GetArray(5))
    # Interpolate
    ui  = griddata(x, u,  pos, method='cubic')
    vi  = griddata(x, v,  pos, method='cubic')
    pi  = griddata(x, p,  pos, method='cubic')
    pxi = griddata(x, px, pos, method='cubic')
    pyi = griddata(x, py, pos, method='cubic')
    si  = griddata(x, s,  pos, method='cubic')
    h5_data.create_dataset('u' +str(t-500), data=ui .astype(np.float32))
    h5_data.create_dataset('v' +str(t-500), data=vi .astype(np.float32))
    h5_data.create_dataset('p' +str(t-500), data=pi .astype(np.float32))
    h5_data.create_dataset('px'+str(t-500), data=pxi.astype(np.float32))
    h5_data.create_dataset('py'+str(t-500), data=pyi.astype(np.float32))
    h5_data.create_dataset('s' +str(t-500), data=si .astype(np.float32))

# Store edges position
Ne = len(edges)
e = np.zeros((Ne,2), dtype=np.int64)
for i, edge in enumerate(edges):
    e[i,:] = edge.text.split()[:2]

# Store edges in boundaries
i_boundaries = []
for i in [1,3,4]:
    i_boundaries.append([int(num) for num in composites[i].text[3:-2].split(',')])
o_boundaries = []
for i in [2]:
    o_boundaries.append([int(num) for num in composites[i].text[3:-2].split(',')])
w_boundaries = []
for i in [5]:
    w_boundaries.append([int(num) for num in composites[i].text[3:-2].split(',')])
# Set boundary
omega = np.zeros(Nv, dtype=np.uint8)
for boundary in i_boundaries:
    omega[e[boundary, 0]] = 2
    omega[e[boundary, 1]] = 2
for boundary in o_boundaries:
    omega[e[boundary, 0]] = 3
    omega[e[boundary, 1]] = 3
for boundary in w_boundaries:
    omega[e[boundary, 0]] = 4
    omega[e[boundary, 1]] = 4
h5_data.create_dataset('omega', data=omega)

h5_data.create_dataset('Re', data=args.Re)

h5_data.close()