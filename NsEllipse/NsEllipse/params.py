import numpy as np
import sys
import random

# Re
# Re = 1500
r = random.random()
if r < 0.8:   
    Re    = round(np.random.uniform(500, 1000), 0)
elif r < 0.9:
    Re    = round(np.random.uniform(500,  550), 0)
else:
    Re    = round(np.random.uniform(950, 1000), 0)

# b
# b = random.choice([0.1, 1])
r = random.random()
if r < 0.8:   
    b     = round(np.random.uniform(0.50, 0.80), 3)
elif r < 0.9:
    b     = round(np.random.uniform(0.50, 0.55), 3)
else:
    b     = round(np.random.uniform(0.75, 0.80), 3)

# h
# h = random.choice([0.1, 0.2])
r = random.random()
if r < 0.8:   
    h     = round(np.random.uniform(0.10, 0.16), 3)
elif r < 0.9:
    h     = round(np.random.uniform(0.10, 0.11), 3)
else:
    h     = round(np.random.uniform(0.15, 0.16), 3)

# H
# H = 5
r = random.random()
if r < 0.8:   
    H     = round(np.random.uniform(5.0, 6.0), 2)
elif r < 0.9:
    H     = round(np.random.uniform(5.0, 5.1), 2)
else:
    H     = round(np.random.uniform(5.9, 6.0), 2)


original_stdout = sys.stdout # Save a reference to the original standard output
with open('./params', 'w') as f:
    sys.stdout = f # Change the standard output to the file we created.
    print("Re="+str(Re))
    print("b="+str(b))
    print("h="+str(h))
    print("H="+str(H))
    sys.stdout = original_stdout # Reset the standard output to its original value