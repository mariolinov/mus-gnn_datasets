h = £h;
D = 1;
H = £H*D;
W = 8.5*D;
a = 0.5*D;
b = £b*a;
cx = 2*D;
cy = H*0.5;
alpha = £alpha*Pi/180.;

Point(1)  = {    0,    0, 0,     h};
Point(2)  = {    W,    0, 0,     h};
Point(3)  = {    W,    H, 0,     h};
Point(4)  = {    0,    H, 0,     h};
Point(5)  = {   cx,   cy, 0, 0.3*h};
Point(6)  = { cx-a*Cos(alpha), cy+a*Sin(alpha), 0, 0.3*h};
Point(7)  = { cx+a*Cos(alpha), cy-a*Sin(alpha), 0, 0.3*h};
Point(8)  = { cx+b*Sin(alpha), cy+b*Cos(alpha), 0, 0.3*h};
Point(9)  = { cx-b*Sin(alpha), cy-b*Cos(alpha), 0, 0.3*h};

Line(1) = { 1, 2};
Line(2) = { 2, 3};
Line(3) = { 3, 4};
Line(4) = { 4, 1};

Ellipse(5) = {6, 5, 6, 8};
Ellipse(6) = {8, 5, 7, 7};
Ellipse(7) = {7, 5, 7, 9};
Ellipse(8) = {9, 5, 6, 6};

Line Loop(1) = {1, 2, 3, 4, 5, 6, 7, 8};
Plane Surface(1) = {1};
Physical Surface(1) = {1};

Physical Line(100) = {1};
Physical Line(200) = {2};
Physical Line(300) = {3};
Physical Line(400) = {4};
Physical Line(500) = {5, 6, 7, 8};