import numpy as np
import sys
import argparse


parser = argparse.ArgumentParser()
parser.add_argument('case', type=int)
args = parser.parse_args()

Re    = np.array([500, 600, 700, 800, 900, 1000])
alpha = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
b     = np.array([0.5, 0.6, 0.7, 0.8])
h     = 0.12
H     = 5.5

cases = np.array(np.meshgrid(Re,alpha,b)).T.reshape(-1,3)
case = cases[args.case-1]
Re = case[0]
alpha = case[1]
b = case[2]

original_stdout = sys.stdout # Save a reference to the original standard output
with open('./params', 'w') as f:
    sys.stdout = f # Change the standard output to the file we created.
    print("Re="+str(Re))
    print("alpha="+str(alpha))
    print("b="+str(b))
    print("h="+str(h))
    print("H="+str(H))
    sys.stdout = original_stdout # Reset the standard output to its original value