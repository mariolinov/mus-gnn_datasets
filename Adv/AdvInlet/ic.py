import numpy as np
from scipy.interpolate import interp2d

nx = Pnx
ny = Pny
Nx = 1024
Ny = 1024

x = np.linspace(0,   1, Nx)
y = np.linspace(0, 0.5, Ny)
X, Y = np.meshgrid(x,y)
u = np.zeros_like(X)
for i in range(nx):
    for j in range(ny):
        aux = 2.*np.pi*(i*X+j*Y/0.5)*1j
        u += np.random.rand(1)*np.real(np.exp(aux))

u = u*np.exp(-2*( (X-0.5)**2 + (Y-0.25)**2 ))
u = 2*((u - u.min())/(u.max()-u.min()))-1.

# import matplotlib.pyplot as plt
# plt.imshow(u, cmap="coolwarm")
# plt.show()


f = open("ic.pts", "w")
f.write('<?xml version="1.0" encoding="utf-8"?> \n')
f.write('<NEKTAR> \n')
f.write('<POINTS DIM="2" FIELDS="u"> \n')
for i in range(Nx):
    for j in range(Ny):
        f.write('{}   {}   {} \n'.format(x[i], y[j], u[i,j]))
f.write('</POINTS> \n')
f.write('</NEKTAR>')
f.close()