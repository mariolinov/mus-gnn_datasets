import numpy as np
import sys


eps = np.random.uniform()
if eps <= 0.7:
    length = np.sqrt(np.random.uniform(0., 1.))
else:
    length = 1.
angle  = np.random.uniform(0., 0.785398)
x = length*np.cos(angle)
y = length*np.sin(angle)

mu  = np.random.uniform()

if mu <= 0.7:
    nx= np.random.randint(3,9)
    ny= np.random.randint(2,5)
elif mu <= 0.85:
    nx= 3
    ny= 2
else:
    nx= 8
    ny= 4

original_stdout = sys.stdout # Save a reference to the original standard output
with open('adv_coef', 'w') as f:
    sys.stdout = f # Change the standard output to the file we created.
    print("Pax="+str(x))
    print("Pay="+str(y))
    print("Pnx=" +str(nx))
    print("Pny=" +str(ny))
    sys.stdout = original_stdout # Reset the standard output to its original value