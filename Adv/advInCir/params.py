import numpy as np
import sys


magnitude = np.sqrt(np.random.uniform(0.2**2, 0.75**2))
angle  = np.random.uniform(-0.785398, 0.785398)
u = magnitude*np.cos(angle)
v = magnitude*np.sin(angle)
nx = np.random.randint(3,7)
ny = np.random.randint(3,7)

original_stdout = sys.stdout # Save a reference to the original standard output
with open('params', 'w') as f:
    sys.stdout = f # Change the standard output to the file we created.
    print("Pu="+str(u))
    print("Pv="+str(v))
    print("Pnx="+str(nx))
    print("Pny="+str(ny))
    sys.stdout = original_stdout # Reset the standard output to its original value