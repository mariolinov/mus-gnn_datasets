import numpy as np
import sys

### Params ###

# Radios
R1 = 0.25
R2 = 0.5
# Omegas
v1 = np.random.uniform(-1.0, 1.0)
v2 = np.random.uniform(-1.0, 1.0)
W1 = v1/R1
W2 = v2/R2

# Write in params
original_stdout = sys.stdout # Save a reference to the original standard output
with open('params', 'w') as f:
    sys.stdout = f # Change the standard output to the file we created.
    print("PR1="+str(R1))
    print("PR2="+str(R2))
    print("PW1="+str(W1))
    print("PW2="+str(W2))
    print("Pnx="+str(np.random.randint(3,6)))
    print("Pny="+str(np.random.randint(3,6)))
    sys.stdout = original_stdout # Reset the standard output to its original value


### Advection field for Taylor-Couette ###

N = 1024
r = np.linspace(R1, R2, N)
theta = np.linspace(0, 2*np.pi, N)
r, theta = np.meshgrid(r, theta)
x = r*np.cos(theta)
y = r*np.sin(theta)
A =  (W1*R1**2 - W2*R2**2)/(R1**2 - R2**2)
B = -(R1**2*R2**2*(W1 - W2))/(R1**2 - R2**2)
u = A*r + B/r
ax = -u*np.sin(theta)
ay =  u*np.cos(theta)

# Write to ax.pts and ay.pts
f = open("ax.pts", "w")
f.write('<?xml version="1.0" encoding="utf-8"?> \n')
f.write('<NEKTAR> \n')
f.write('<POINTS DIM="2" FIELDS="Vx"> \n')
for i in range(N):
    for j in range(N):
        f.write('{}  {}  {} \n'.format(x[i,j], y[i,j], ax[i,j]))
f.write('</POINTS> \n')
f.write('</NEKTAR>')
f.close()

f = open("ay.pts", "w")
f.write('<?xml version="1.0" encoding="utf-8"?> \n')
f.write('<NEKTAR> \n')
f.write('<POINTS DIM="2" FIELDS="Vy"> \n')
for i in range(N):
    for j in range(N):
        f.write('{}  {}  {} \n'.format(x[i,j], y[i,j], ay[i,j]))
f.write('</POINTS> \n')
f.write('</NEKTAR>')
f.close()