h = 0.01;
delta = 0.01;
R1 = 0.25-delta;
R2 = 0.5+delta;

Point(1)  = {   0, 0, 0, h};
Point(2)  = {  R1, 0, 0, h};
Point(3)  = { -R1, 0, 0, h};
Point(4)  = {  R2, 0, 0, h};
Point(5)  = { -R2, 0, 0, h};

Circle(1) = {2, 1, 3};
Circle(2) = {3, 1, 2};
Circle(3) = {4, 1, 5};
Circle(4) = {5, 1, 4};

Line Loop(1) = {1, 2};
Line Loop(2) = {3, 4};
Plane Surface(1) = {1,2};

Physical Line(100) = {1,2};
Physical Line(200) = {3,4};
Physical Surface(1) = {1};
