import numpy as np
import sys


u  = np.random.uniform(0.2, 0.75)
theta = np.random.uniform(-np.pi/4, np.pi/4)
nx = np.random.randint(3,7)
ny = np.random.randint(3,7)

original_stdout = sys.stdout # Save a reference to the original standard output
with open('params', 'w') as f:
    sys.stdout = f # Change the standard output to the file we created.
    print("Pu="+str(u))
    print("Ptheta="+str(theta))
    print("Pnx="+str(nx))
    print("Pny="+str(ny))
    sys.stdout = original_stdout # Reset the standard output to its original value