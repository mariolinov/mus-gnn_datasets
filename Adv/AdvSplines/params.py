import numpy as np
import sys


u  = np.random.uniform(0.2, 0.75)
nx = np.random.randint(3,7)
ny = np.random.randint(3,7)

def circle(theta):
    pt = np.zeros(2)
    theta = np.deg2rad(theta + np.random.uniform(-30,30))
    R     = np.random.uniform(0.1,0.3)
    pt[0] = 0.5+R*np.cos(theta)
    pt[1] = 0.5+R*np.sin(theta)
    return pt
pt = []
thetas = np.arange(0,360,60)
for theta in thetas:
    pt.append(circle(theta))

original_stdout = sys.stdout # Save a reference to the original standard output
with open('params', 'w') as f:
    sys.stdout = f # Change the standard output to the file we created.
    print("Pu="+str(u))
    print("Pnx="+str(nx))
    print("Pny="+str(ny))
    i = 11
    for p in pt:
        print("point"+str(i)+"x"+"="+str(p[0]))
        print("point"+str(i)+"y"+"="+str(p[1]))
        i += 1
    sys.stdout = original_stdout # Reset the standard output to its original value