h = 0.01;
H = 1;
W = H;
cx = W*0.5;
cy = H/2;
a = 0.2*H;
b = 0.1*H;

Point(1)   = { 0, 0, 0, h};
Point(2)   = { W, 0, 0, h};
Point(3)   = { W, H, 0, h};
Point(4)   = { 0, H, 0, h};
Point(11)  = { £11x, £11y, 0, 0.5*h};
Point(12)  = { £12x, £12y, 0, 0.5*h};
Point(13)  = { £13x, £13y, 0, 0.5*h};
Point(14)  = { £14x, £14y, 0, 0.5*h};
Point(15)  = { £15x, £15y, 0, 0.5*h};
Point(16)  = { £16x, £16y, 0, 0.5*h};

Line(1) = { 1, 2};
Line(2) = { 2, 3};
Line(3) = { 3, 4};
Line(4) = { 4, 1};
BSpline(5) = {11, 12, 13, 14, 15, 16, 11};

Line Loop(1) = {1, 2, 3, 4, 5};
Plane Surface(1) = {1};
Physical Surface(1) = {1};

Physical Line(100) = {1};
Physical Line(200) = {2};
Physical Line(300) = {3};
Physical Line(400) = {4};
Physical Line(500) = {5, 6, 7, 8};

Periodic Line {1} = {3} Translate{0,H,0};
Periodic Line {2} = {4} Translate{W,0,0};
