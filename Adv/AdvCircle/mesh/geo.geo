h = 0.01;
H = 1;
W = H;
cx = W*0.5;
cy = H/2;
R = 0.1*H;

Point(1)  = { 0, 0, 0, h};
Point(2)  = { W, 0, 0, h};
Point(3)  = { W, H, 0, h};
Point(4)  = { 0, H, 0, h};
Point(5)  = { cx, cy, 0, 0.5*h};
Point(6)  = { cx+R, cy, 0, 0.5*h};
Point(7)  = { cx-R, cy, 0, 0.5*h};

Line(1) = { 1, 2};
Line(2) = { 2, 3};
Line(3) = { 3, 4};
Line(4) = { 4, 1}; 
Circle(5) = {6, 5, 7};
Circle(6) = {7, 5, 6};

Line Loop(1) = {1, 2, 3, 4, 5, 6};
Plane Surface(1) = {1};
Physical Surface(1) = {1};

Physical Line(100) = {1};
Physical Line(200) = {2};
Physical Line(300) = {3};
Physical Line(400) = {4};
Physical Line(500) = {5,6};

Periodic Line {1} = {3} Translate{0,H,0};
Periodic Line {2} = {4} Translate{W,0,0};
