h = 0.01;
H = 1;
W = H;
cx = W*0.5;
cy = H/2;
R = 0.1*H;

Point(1)  = { 0, 0, 0, h};
Point(2)  = { W, 0, 0, h};
Point(3)  = { W, H, 0, h};
Point(4)  = { 0, H, 0, h};
Point(5)  = { cx-R, cy-R, 0, 0.5*h};
Point(6)  = { cx+R, cy-R, 0, 0.5*h};
Point(7)  = { cx+R, cy+R, 0, 0.5*h};
Point(8)  = { cx-R, cy+R, 0, 0.5*h};

Line(1) = { 1, 2};
Line(2) = { 2, 3};
Line(3) = { 3, 4};
Line(4) = { 4, 1};
Line(5) = { 5, 6};
Line(6) = { 6, 7};
Line(7) = { 7, 8};
Line(8) = { 8, 5}; 


Line Loop(1) = {1, 2, 3, 4, 5, 6, 7, 8};
Plane Surface(1) = {1};
Physical Surface(1) = {1};

Physical Line(100) = {1};
Physical Line(200) = {2};
Physical Line(300) = {3};
Physical Line(400) = {4};
Physical Line(500) = {5,6,7,8};

Periodic Line {1} = {3} Translate{0,H,0};
Periodic Line {2} = {4} Translate{W,0,0};
