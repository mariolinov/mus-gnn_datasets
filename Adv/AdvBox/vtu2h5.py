import h5py
import numpy as np
import vtk
from vtk.util.numpy_support import vtk_to_numpy
from scipy.interpolate import griddata
import argparse
from xml.etree import ElementTree


# Inputs to the program
parser = argparse.ArgumentParser()
parser.add_argument('mesh_file',type=str)
parser.add_argument('h5_file',type=str)
parser.add_argument('ax',type=float)
parser.add_argument('ay',type=float)
args = parser.parse_args()

h5_data = h5py.File(args.h5_file, 'w')

# Get nodes and edges from xml file
dom = ElementTree.parse(args.mesh_file)
V = dom.findall('GEOMETRY/VERTEX/V')
edges = dom.findall('GEOMETRY/EDGE/E')
composites = dom.findall('GEOMETRY/COMPOSITE/C')
# Store nodes position
Nv = len(V)
pos = np.zeros((Nv,2))
for i, node in enumerate(V):
    pos[i,:] = node.text.split()[:2]
# Save points coordinates
h5_data.create_dataset('pos', data=pos.astype(np.float32))

Nt=100
for t in range(Nt):
    # Get data from vtu
    vtu_file = "temp_"+str(t)+".vtu"
    reader = vtk.vtkXMLUnstructuredGridReader()
    reader.SetFileName(vtu_file)
    reader.Update()
    data = reader.GetOutput()
    if t == 0:
        points = data.GetPoints()
        x = vtk_to_numpy(points.GetData())[:,:2]
    u = vtk_to_numpy(data.GetPointData().GetArray(0))
    # Interpolate
    ui = griddata(x, u, pos, method='cubic')
    # Save u
    h5_data.create_dataset('u'+str(t), data=ui.astype(np.float32))

# Store edges' position
Ne = len(edges)
e = np.zeros((Ne,2), dtype=np.int64)
for i, edge in enumerate(edges):
    e[i,:] = edge.text.split()[:2]
# Store edges in boundaries
boundaries = []
for i in [1,2,3,4]:
    boundaries.append([int(num) for num in composites[i].text[3:-2].split(',')])
# Set boundary
omega = np.zeros(Nv, dtype=np.uint8)
for boundary in boundaries:
    omega[e[boundary, 0]] = 1
    omega[e[boundary, 1]] = 1
h5_data.create_dataset('omega', data=omega)

# Save ax, ay and b
h5_data.create_dataset('params', data=np.array([args.ax, args.ay], dtype=np.float32))

h5_data.close()