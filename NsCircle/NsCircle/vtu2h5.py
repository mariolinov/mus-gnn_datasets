import h5py
import numpy as np
import vtk
from vtk.util.numpy_support import vtk_to_numpy
from scipy.interpolate import griddata
import argparse
from xml.etree import ElementTree


# Inputs to the program
parser = argparse.ArgumentParser()
parser.add_argument('h5_file',type=str)
parser.add_argument('Re',type=float)
args = parser.parse_args()

print("Writingfile:", args.h5_file)
h5_data = h5py.File(args.h5_file, 'w')

# Get nodes and edges from xml file
dom = ElementTree.parse("mod_geo.xml")
V = dom.findall('GEOMETRY/VERTEX/V')
edges = dom.findall('GEOMETRY/EDGE/E')
composites = dom.findall('GEOMETRY/COMPOSITE/C')
# Store nodes position
Nv = len(V)
pos = np.zeros((Nv,2))
for i, node in enumerate(V):
    pos[i,:] = node.text.split()[:2]
# Save points coordinates
h5_data.create_dataset('pos', data=pos.astype(np.float32))
upper = (pos[:,1] == pos[:,1].max())
upper_sorted = np.argsort(pos[upper,0])
lower = (pos[:,1] == pos[:,1].min())
lower_sorted = np.argsort(pos[lower,0])

Nt=400
for t in range(Nt):
    # Get data from vtu
    vtu_file = "temp_"+str(t+1)+".vtu"
    reader = vtk.vtkXMLUnstructuredGridReader()
    reader.SetFileName(vtu_file)
    reader.Update()
    data = reader.GetOutput()
    if t == 0:
        points = data.GetPoints()
        x = vtk_to_numpy(points.GetData())[:,:2]
    u = vtk_to_numpy(data.GetPointData().GetArray(0))
    v = vtk_to_numpy(data.GetPointData().GetArray(1))
    p = vtk_to_numpy(data.GetPointData().GetArray(2))
    # Interpolate
    ui = griddata(x, u, pos, method='cubic')
    vi = griddata(x, v, pos, method='cubic')
    pi = griddata(x, p, pos, method='cubic')
    # Periodicity
    u_upper = ui[upper]
    u_upper[upper_sorted] = ui[lower][lower_sorted]
    ui[upper] = u_upper
    v_upper = vi[upper]
    v_upper[upper_sorted] = vi[lower][lower_sorted]
    vi[upper] = v_upper
    p_upper = pi[upper]
    p_upper[upper_sorted] = pi[lower][lower_sorted]
    pi[upper] = p_upper
    # Save p, u, v
    h5_data.create_dataset('u'+str(t), data=ui.astype(np.float32))
    h5_data.create_dataset('v'+str(t), data=vi.astype(np.float32))
    h5_data.create_dataset('p'+str(t), data=pi.astype(np.float32))

# Store edges position
Ne = len(edges)
e = np.zeros((Ne,2), dtype=np.int64)
for i, edge in enumerate(edges):
    e[i,:] = edge.text.split()[:2]

# Store edges in boundaries
p_boundaries = []
for i in [1,3]:
    p_boundaries.append([int(num) for num in composites[i].text[3:-2].split(',')])
i_boundaries = []
for i in [4]:
    i_boundaries.append([int(num) for num in composites[i].text[3:-2].split(',')])
o_boundaries = []
for i in [2]:
    o_boundaries.append([int(num) for num in composites[i].text[3:-2].split(',')])
w_boundaries = []
for i in [5]:
    w_boundaries.append([int(num) for num in composites[i].text[3:-2].split(',')])
# Set boundary
omega = np.zeros(Nv, dtype=np.uint8)
for boundary in p_boundaries:
    omega[e[boundary, 0]] = 1
    omega[e[boundary, 1]] = 1
for boundary in i_boundaries:
    omega[e[boundary, 0]] = 2
    omega[e[boundary, 1]] = 2
for boundary in o_boundaries:
    omega[e[boundary, 0]] = 3
    omega[e[boundary, 1]] = 3
for boundary in w_boundaries:
    omega[e[boundary, 0]] = 4
    omega[e[boundary, 1]] = 4
h5_data.create_dataset('omega', data=omega)

h5_data.create_dataset('Re', data=args.Re)

h5_data.close()