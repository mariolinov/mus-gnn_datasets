import numpy as np
import sys

Re = round(np.random.uniform(100, 500), 0)
H  = round(np.random.uniform(4, 6), 2)

original_stdout = sys.stdout # Save a reference to the original standard output
with open('./params', 'w') as f:
    sys.stdout = f # Change the standard output to the file we created.
    print("Re="+str(Re))
    print("H=" +str(H) )
    sys.stdout = original_stdout # Reset the standard output to its original value