h = 0.12*£H/4;
D = 1;
H = £H*D;
W = 7*D;
cx = 1.5*D;
cy = H*0.5;
R = 0.5*D;

Point(1)  = { 0, 0, 0, h};
Point(2)  = { W, 0, 0, 0.7*h};
Point(3)  = { W, H, 0, 0.7*h};
Point(4)  = { 0, H, 0, h};
Point(5)  = { cx, cy, 0, 0.2*h};
Point(6)  = { cx+R, cy, 0, 0.1*h};
Point(7)  = { cx-R, cy, 0, 0.2*h};

Line(1) = { 1, 2};
Line(2) = { 2, 3};
Line(3) = { 3, 4};
Line(4) = { 4, 1}; 
Circle(5) = {6, 5, 7};
Circle(6) = {7, 5, 6};

Line Loop(1) = {1, 2, 3, 4, 5, 6};
Plane Surface(1) = {1};
Physical Surface(1) = {1};

Physical Line(100) = {1};
Physical Line(200) = {2};
Physical Line(300) = {3};
Physical Line(400) = {4};
Physical Line(500) = {5,6};

Periodic Line {1} = {-3};
