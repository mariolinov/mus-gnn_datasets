h = 0.12*£H/4;
D = 1;
H = £H*D;
W = 7*D;
cx = 1.5*D;
cy = H*0.5;
R = 0.5*D;

Point(1)  = { 0, 0, 0, h};
Point(2)  = { W, 0, 0, 0.7*h};
Point(3)  = { W, H, 0, 0.7*h};
Point(4)  = { 0, H, 0, h};
Point(5)  = { cx, cy, 0, 0.2*h};
Point(6)  = { cx+R, cy, 0, 0.1*h};
Point(7)  = { cx-R, cy, 0, 0.2*h};
Point(11)  = { 1.5*W, 0, 0, 5*h};
Point(12)  = { 1.5*W, H, 0, 5*h};

Line(1) = { 1, 2};
Line(2) = { 2, 3};
Line(3) = { 3, 4};
Line(4) = { 4, 1}; 
Circle(5) = {6, 5, 7};
Circle(6) = {7, 5, 6};
Line(11) = { 2, 11};
Line(12) = { 11, 12}; 
Line(13) = { 12, 3};

Line Loop(1) = {1, 2, 3, 4, 5, 6};
Plane Surface(1) = {1};
Physical Surface(1) = {1};

Line Loop(2) = {11, 12, 13, -2};
Plane Surface(2) = {2};
Physical Surface(2) = {2};

Physical Line(100) = {1,11};
Physical Line(200) = {12};
Physical Line(300) = {13,3};
Physical Line(400) = {4};
Physical Line(500) = {5,6};

Periodic Line {1}  = {-3};
Periodic Line {11} = {-13};
